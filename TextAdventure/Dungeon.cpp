#include <iostream>
#include "Dungeon.h"
#include "Player.h"

using namespace std;

Dungeon::Dungeon(Player p)
{
	player = p;
}

void Dungeon::printActions(int numActions, string actions[])
{
	cout << "Choose an action:\n";

	for (int i = 0; i < numActions; i++)
	{
		cout << actions[i] << "\n";
	}
}

void Dungeon::handleFightActions(GameCharacter* enemy)
{
	//set actions
	string actions[] =
	{
		"attack. Attack the monster",
		"retreat. Back to the previous room"
	};

	while (true)
	{
		//create actions
		printActions(2, actions);

		//read player input
		string input;
		cin >> input;

		//player actions
		if (input == "attack")
		{
			//attack
			int damage = enemy->takeDamage(player.attack);
			cout << "You attack does " << damage << " damage.\n";
			cout << "Enemy now have " << enemy->currentHealth << " health.\n";
		}
		else if (input == "retreat")
		{
			//retreat
			player.changeRooms(player.previousRoom);
			enterRoom(player.currentRoom);
			return;
		}
		else
		{
			cout << "Incorrect choice.\n";
		}

#pragma region Check if enemy is dead
		if (enemy->checkIsDead())
		{
			cout << "You win! You have defeated the " << enemy->name << ".\n";
			//player level up
			player.increaseStats(10, 5, 5);
			player.levelUp++;
			cout << "Level up! You now have " << player.currentHealth << " health, " << player.attack << " attack, " << player.defence << " defence.\n";
			//clear room
			player.currentRoom->clearEnemies();
			return;
		}
#pragma endregion

#pragma region Enemy actions
		//attack
		int damage = player.takeDamage(enemy->attack);
		cout << enemy->name << "'s attack does " << damage << " damage.\n";
		cout << "You now have " << player.currentHealth << " health.\n";
		//check if player is dead
		if (player.checkIsDead())
		{
			return;
		}
#pragma endregion	
	}
}

void Dungeon::handleRoomWithEnemy(Room* room)
{
	//get enemy
	GameCharacter enemy = room->enemies.front();

#pragma region Enemy Rooms Message
	//room 1
	if (room->pos == 1)
	{
		cout << "You notice two passage: north and east. You also notice little hatched eggs on the floor with some spiders blocking your way, you know what to do!\n";
	}
	//room 3
	else if (room->pos == 3)
	{
		cout << "You notice a big strange thing blocking the east passage... Oh my god, it's a big spider! Disgusting!\n";
	}
	//room 6
	else if (room->pos == 6)
	{
		cout << "Probabily this is the last room by it's size: very large and with a lot of skulls in the middle. At the end of the room you can see the exit of the cave but in a blink of an eye, from the pile of skulls, a wild minotaur appears!\n";
	}
	//room 8
	else if (room->pos == 8)
	{
		cout << "There is only passage direct to north but you can feel your body freezing in this room like being inside of a freezer. With your sixth sense, you can see clearly a wrait making jokes in front of you. With your anger, you prepare yourself for the battle!\n";
	}

#pragma endregion

	//set actions
	string actions[] =
	{
		"fight. Begin the battle",
		"inspect. Inspect the creature",
		"retreat. Back to the previous room"
	};

	while (true)
	{
		//create actions
		printActions(3, actions);

		//read player input
		string input;
		cin >> input;

		//read choice
		if (input == "fight")
		{
			//fight enemy
			handleFightActions(&enemy);
			return;
		}
		else if (input == "inspect")
		{
			//message
			cout << enemy.name << " has " << enemy.currentHealth << " health, " << enemy.attack << " attack and " << enemy.defence << " defence.\n";
		}
		else if (input == "retreat")
		{
			//back to previous room
			player.changeRooms(player.previousRoom);
			enterRoom(player.currentRoom);
			return;
		}
		else
		{
			cout << "Incorrect choice.\n";
		}
	}
}

void Dungeon::handleLootActions(Room* room)
{
	//get item
	Item item = room->items.front();
	int size = room->items.size();

	//loot items
	player.lootRoom(room);
	room->clearLoot();

	for (int i = 0; i < size; i++)
	{
		//room 2
		if (room->pos == 2)
		{
			cout << "You open the chest and find a " << item.name << ".\n";
		}
		//room 5
		else if (room->pos == 5)
		{
			cout << "You remove with accuracy the " << item.name << " from the mystical monument.\n";
			player.isKey = true;
		}

		//if room with the sword, show message
		if (item.name == "Sword")
		{
			cout << "Your attack is now " << player.attack << ".\n";
		}
	}
}

void Dungeon::handleRoomWithItems(Room* room)
{
	//room 2
	if (room->pos == 2)
	{
		//intial message
		cout << "You enter the room and see a large chest in the middle.\n";
		//set actions
		string actions[] =
		{
			"loot. pick up the curio",
			"move. Move to another room"
		};

		while (true)
		{
			//create actions
			printActions(2, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "loot")
			{
				//loot chest
				handleLootActions(room);
				return;
			}
			else if (input == "move")
			{
				//move
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
	}
	//room 5
	else if (room->pos == 5)
	{
		//intial message
		cout << "You enter the room and see an ancient monument, at the top you can see a shiny thing. Wait... i can see...you found the key! Now back to the locked room!\n";
		//set actions
		string actions[] =
		{
			"take. Pick up the key",
			"move. Move to another room"
		};

		while (true)
		{
			//create actions
			printActions(2, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "take")
			{
				//loot chest
				handleLootActions(room);
				return;
			}
			else if (input == "move")
			{
				//move
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
	}
}

void Dungeon::handleEmptyRoom(Room* room)
{
#pragma region Empty Rooms Message
	//room 0
	if (room->pos == 0)
	{
		cout << "The entrance of the cave where you start.\n";
	}
	//room 4
	else if (room->pos == 4)
	{
		cout << "There is a dark room, you can feel someone presence... something is whispering between your ears.\n";
	}
	//room 7
	else if (room->pos == 7)
	{
		cout << "A colorfull room with some kind of crystals.\n";
	}
	//other rooms
	else
	{
		cout << "Nothing new, you already explore this room.\n";
	}
#pragma endregion

	//set actions
	string actions[] =
	{
		"move. Move to another room",
		"examine. Examine the room",
		"stats. Check your stats",
		"rest. Regain all your health"
	};

	while (true)
	{
		//create actions
		printActions(4, actions);

		//read player input
		string input;
		cin >> input;

		//read choice
		if (input == "move")
		{
			//move
			return;
		}
		else if (input == "examine")
		{
			//message room 0
			if (room->pos == 0)
			{
				cout << "It's the entrance of the cave, nothing special. You notice two passage: one direct to east and the other direct to south.\n";
			}
			//message room 4
			else if (room->pos == 4)
			{
				cout << "You definitely can sense the presence of someone in this room. The whispering start to be more loud for every second you pass in this room and at the same time you slowly start to freeze. Perhaps, it's time to leave the room if i can suggest to you.\n";
			}
			//message room 4
			else if (room->pos == 7)
			{
				cout << "It's an ancient room indeed! There are small and medium colorfull crystals anywhere in the room, on the floor, on the ceiling, on the walls... but you notice something... Wait, did the crystals on the wall move?\n";
			}
		}
		else if (input == "stats")
		{
			//message
			cout << "Your health is " << player.currentHealth << ", attack " << player.attack << " and defence " << player.defence << ".\n";
		}
		else if (input == "rest")
		{
			//check rest
			if (!player.isRest)
			{
				if (player.levelUp == 0)
				{
					player.currentHealth = 100;
					cout << "You regain " << player.currentHealth << "!\n";
					player.isRest = true;
				}
				else if (player.levelUp == 1)
				{
					player.currentHealth = 110;
					cout << "You regain " << player.currentHealth << "!\n";
					player.isRest = true;
				}
				else if (player.levelUp == 2)
				{
					player.currentHealth = 120;
					cout << "You regain " << player.currentHealth << "!\n";
					player.isRest = true;
				}
				else if (player.levelUp == 3)
				{
					player.currentHealth = 130;
					cout << "You regain " << player.currentHealth << "!\n";
					player.isRest = true;
				}
			}
			else
			{
				cout << "You already rest! I'm sorry.\n";
			}
		}
		else
		{
			cout << "Incorrect choice.\n";
		}
	}
}

void Dungeon::enterRoom(Room* room)
{
	if (room->enemies.size() != 0)
	{
		//room with enemy
		handleRoomWithEnemy(room);
	}
	else if (room->items.size() != 0)
	{
		//room with chest
		handleRoomWithItems(room);
	}
	else
	{
		//empty room
		handleEmptyRoom(room);
	}
}

void Dungeon::handleMovementActions(Room* room)
{
	while (true)
	{
		//if room 0
		if (room->pos == 0)
		{
			string actions[] =
			{
				"E. Move east",
				"S. Move south",
			};

			//create actions
			printActions(2, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "E")
			{
				player.changeRooms(&rooms[1]);
				return;
			}
			else if (input == "S")
			{
				player.changeRooms(&rooms[3]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 1
		else if (room->pos == 1)
		{
			string actions[] =
			{
				"E. Move east",
				"S. Move south",
				"O. Move ovest"
			};

			//create actions
			printActions(3, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "E")
			{
				player.changeRooms(&rooms[2]);
				return;
			}
			else if (input == "S")
			{
				player.changeRooms(&rooms[4]);
				return;
			}
			else if (input == "O")
			{
				player.changeRooms(&rooms[0]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 2
		else if (room->pos == 2)
		{
			string actions[] =
			{
				"O. Move ovest"
			};

			//create actions
			printActions(1, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "O")
			{
				player.changeRooms(&rooms[1]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 3
		else if (room->pos == 3)
		{
			string actions[] =
			{
				"N. Move north",
				"E. Move east"
			};

			//create actions
			printActions(2, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "N")
			{
				player.changeRooms(&rooms[0]);
				return;
			}
			else if (input == "E")
			{
				player.changeRooms(&rooms[4]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 4
		else if (room->pos == 4)
		{
			string actions[] =
			{
				"N. Move north",
				"O. Move ovest",
				"S. Move south"
			};

			//create actions
			printActions(3, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "N")
			{
				player.changeRooms(&rooms[1]);
				return;
			}
			else if (input == "O")
			{
				player.changeRooms(&rooms[3]);
				return;
			}
			else if (input == "S")
			{
				player.changeRooms(&rooms[7]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 5
		else if (room->pos == 5)
		{
			string actions[] =
			{
				"S. Move south"
			};

			//create actions
			printActions(1, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "S")
			{
				player.changeRooms(&rooms[8]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 6
		else if (room->pos == 6)
		{
			string actions[] =
			{
				"E. Move east"
			};

			//create actions
			printActions(1, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "E")
			{
				player.changeRooms(&rooms[7]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 7
		else if (room->pos == 7)
		{
			string actions[] =
			{
				"O. Move ovest",
				"N. Move north",
				"E. Move east"
			};

			//create actions
			printActions(3, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "O")
			{
				if (player.isKey)
				{
					player.changeRooms(&rooms[6]);
					return;
				}
				else
				{
					cout << "The door is locket, you need to find the key first.\n";
				}
			}
			else if (input == "N")
			{
				player.changeRooms(&rooms[4]);
				return;
			}
			else if (input == "E")
			{
				player.changeRooms(&rooms[8]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
		//if room 8
		else
		{
			string actions[] =
			{
				"O. Move ovest",
				"N. Move north"
			};

			//create actions
			printActions(2, actions);

			//read player input
			string input;
			cin >> input;

			//read choice
			if (input == "O")
			{
				player.changeRooms(&rooms[7]);
				return;
			}
			if (input == "N")
			{
				player.changeRooms(&rooms[5]);
				return;
			}
			else
			{
				cout << "Incorrect choice.\n";
			}
		}
	}
}

int Dungeon::runDungeon()
{
	//first message
	cout << player.name << ", will you be ready to explore the mystical dungeon and find the exit? Find the key to open the last room and be ready to face any threat!\n";

	player.currentRoom = &rooms[0];
	player.previousRoom = &rooms[0];

	while (true)
	{
		//enter room
		enterRoom(player.currentRoom);

		//check if player is dead
		if (player.checkIsDead())
		{
			//lose the game
			cout << "Game over! I'm sorry!\n";
			return 0;
		}
		else
		{
			if (player.currentRoom->isExit)
			{
				if (player.currentRoom->enemies.size() == 0)
				{
					//win the game
					cout << "You win! Thanks for playing!\n";
					return 0;
				}
			}
		}
		//move between rooms
		handleMovementActions(player.currentRoom);
	}
}

