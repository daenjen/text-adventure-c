#include <iostream>
#include "Dungeon.h"
#include "Player.h"
#include "GameCharacter.h"
#include "Item.h"
#include "Room.h"

using namespace std;

int main()
{
	/// <summary>
	/// SET UP THE PLAYER
	/// </summary>
	/// <returns></returns>
	cout << "Hello stranger! What is your name?\n";
	string playerName;
	cin >> playerName;
	Player player = Player(playerName, 100, 0, 0);

	/// <summary>
	/// SET UP THE FIRST ROOM
	/// </summary>
	/// <returns></returns>
	Room firstRoom = Room(0, false, vector<Item>(), vector<GameCharacter>());

	/// <summary>
	/// SET UP THE SECOND ROOM
	/// </summary>
	/// <returns></returns>
	GameCharacter firstEnemy = GameCharacter("Spiders", 15, 5, 0);
	vector<GameCharacter> firstRoomEnemies;
	firstRoomEnemies.push_back(firstEnemy);
	Room secondRoom = Room(1, false, vector<Item>(), firstRoomEnemies);

	/// <summary>
	/// SET UP THE THIRD ROOM
	/// </summary>
	/// <returns></returns>
	Item sword = Item("Sword", 0, 10, 0);
	vector<Item> firstRoomItems;
	firstRoomItems.push_back(sword);
	Room thirdRoom = Room(2, false, firstRoomItems, vector<GameCharacter>());

	/// <summary>
	/// SET UP THE FOURTH ROOM
	/// </summary>
	/// <returns></returns>
	GameCharacter secondEnemy = GameCharacter("Big Spider", 60, 15, 5);
	vector<GameCharacter> secondRoomEnemies;
	secondRoomEnemies.push_back(secondEnemy);
	Room fourthRoom = Room(3, false, vector<Item>(), secondRoomEnemies);

	/// <summary>
	/// SET UP THE FIFTH ROOM
	/// </summary>
	/// <returns></returns>
	Room fifthRoom = Room(4, false, vector<Item>(), vector<GameCharacter>());

	/// /// <summary>
	/// SET UP THE SIXTH ROOM
	/// </summary>
	/// <returns></returns>
	Item key = Item("Strange Key", 0, 0, 0);
	vector<Item> keyRoom;
	keyRoom.push_back(key);
	Room sixthRoom = Room(5, false, keyRoom, vector<GameCharacter>());

	/// <summary>
	/// SET UP THE SEVENTH ROOM
	/// </summary>
	/// <returns></returns>
	GameCharacter bossEnemy = GameCharacter("Minotaur Monster", 100, 30, 5);
	vector<GameCharacter> finalBossRoom;
	finalBossRoom.push_back(bossEnemy);
	Room seventhRoom = Room(6, true, vector<Item>(), finalBossRoom);

	/// /// <summary>
	/// SET UP THE EIGHTH ROOM
	/// </summary>
	/// <returns></returns>
	Room eighthRoom = Room(7, false, vector<Item>(), vector<GameCharacter>());

	/// /// <summary>
	/// SET UP THE NINTH ROOM
	/// </summary>
	/// <returns></returns>
	GameCharacter wraithEnemy = GameCharacter("Wrait Monster", 75, 25, 0);
	vector<GameCharacter> thirdRoomEnemies;
	thirdRoomEnemies.push_back(wraithEnemy);
	Room ninthRoom = Room(8, false, vector<Item>(), thirdRoomEnemies);

	/// <summary>
	/// SET DUNGEON
	/// </summary>
	/// <returns></returns>
	Dungeon dungeon = Dungeon(player);
	dungeon.rooms[0] = firstRoom;
	dungeon.rooms[1] = secondRoom;
	dungeon.rooms[2] = thirdRoom;
	dungeon.rooms[3] = fourthRoom;
	dungeon.rooms[4] = fifthRoom;
	dungeon.rooms[5] = sixthRoom;
	dungeon.rooms[6] = seventhRoom;
	dungeon.rooms[7] = eighthRoom;
	dungeon.rooms[8] = ninthRoom;

	//game loop
	while (true)
	{
		int result = dungeon.runDungeon();
		if (result == 0)
		{
			break;
		}
	}
	cout << "Goodbye!";
}