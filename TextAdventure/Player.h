#pragma once
#include "Room.h"


class Player: public GameCharacter
{
public:
	int levelUp = 0;

	bool isRest = false;
	bool isKey=false;

	Room* currentRoom;
	Room* previousRoom;

	std::vector<Item> inventory;

	Player(std::string="", int=0, int=0, int=0);

	void addItem(Item);
	void increaseStats(int, int, int);
	void lootRoom(Room*);
	void changeRooms(Room*);
};


