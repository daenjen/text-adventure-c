#pragma once
#include "Player.h"


class Dungeon
{
public:
	Player player;

	Room rooms[9];

	Dungeon(Player);

	int runDungeon();

	void enterRoom(Room*);
	void handleEmptyRoom(Room*);
	void handleRoomWithItems(Room*);
	void handleRoomWithEnemy(Room*);
	void handleLootActions(Room*);
	void handleFightActions(GameCharacter*);
	void handleMovementActions(Room*);
	void printActions(int, std::string[]);
};
