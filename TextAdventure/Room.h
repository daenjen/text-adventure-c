#pragma once
#include <vector>
#include "Item.h"
#include "GameCharacter.h"


class Room
{
public:
	int pos;

	bool isExit;

	std::vector<Item> items;

	std::vector<GameCharacter> enemies;

	Room(int=0, bool=false, std::vector<Item> = std::vector<Item>(), std::vector<GameCharacter> = std::vector<GameCharacter>());

	void clearLoot();
	void clearEnemies();
};


