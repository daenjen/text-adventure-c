#pragma once
#include <string>


class GameCharacter
{
public:
	std::string name;

	int maxHealth, currentHealth, attack, defence;

	GameCharacter(std::string="", int=0, int=0, int=0);

	int takeDamage(int);

	bool checkIsDead();
};


