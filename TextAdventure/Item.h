#pragma once
#include <string>


class Item
{
public:

	std::string name;

	int health, attack, defence;

	Item(std::string, int, int, int);
};

